class DiceSet
	attr_reader :values

	def roll (times)
		@values = []
		times.times { values << Random.rand(6) + 1 }
		@values
	end
end


class Player
	attr_reader 	:dice_set
	attr_reader 	:player_number
	attr_accessor :score

	def initialize(player_number)
		@dice_set = DiceSet.new
		@score = 0
		@player_number = player_number
	end

	def play(number_of_dices)
		@dice_set.roll(number_of_dices)
	end
end


class Greed
	NUMBER_OF_DICE = 5

	def initialize(number_of_players)
		@players = []
		for number in (1..number_of_players)
			@players << Player.new(number)
		end
	end

	def play
		@players.each do |player|
			puts "player #{player.player_number} begin his turn"
			turn_finished = false
			scoring_dice = 0
			turn_score = 0
			until turn_finished
				dice_set = player.play(NUMBER_OF_DICE - scoring_dice)
				puts "Your dice set was"
				print dice_set
				puts
				if score(dice_set) == 0 #lose the score in this turn
					turn_finished = true
					puts "You lose your turn and your score :-()"
					turn_score = 0
				else
					puts "You score is this roll is #{score(dice_set)}"
					turn_score += score(dice_set)
					scoring_dice = number_of_scoring_dice(dice_set)
					if scoring_dice == dice_set.length # All were scoring_dice
						scoring_dice = 0 # The player will have 5 dice again
					end
					print "Do you like to throw the dice again? (y/n): "
					turn_finished = (gets.chomp == 'y' ? false : true)
				end
			end
			puts "You scored #{turn_score} in this turn"
			player.score += turn_score
			puts "You score is #{player.score}"
		end
	end

	def number_of_scoring_dice(dice_set)
		number_of_scoring_dice = 0
		there_is_a_set = false
		number = 1
		boolean_array = []
		# find a set of any number
		until there_is_a_set or number > 6
			boolean_array = mark_repeated_numbers(dice_set, number, 3)
			there_is_a_set = true unless boolean_array.empty?
			number += 1
		end
		boolean_array = Array.new(dice_set.length, false) if boolean_array.empty?

		if there_is_a_set
			number_of_scoring_dice += 3
		end

		# individual numbers
		dice_set.each_index do |i|
			unless boolean_array[i]
				if dice_set[i] == 1 or dice_set[i] == 5
					number_of_scoring_dice += 1
				end
			end
		end
		number_of_scoring_dice
	end

	def score(dice)
		return 0 if dice.empty?
		score = 0
		thereIsASet = false
		number = 1
		boolean_array = []
		# find a set of any number
		until thereIsASet or number > 6
			boolean_array = mark_repeated_numbers(dice, number, 3)
			thereIsASet = true unless boolean_array.empty?
			number += 1
		end
		boolean_array = Array.new(dice.length, false) if boolean_array.empty?

		if thereIsASet
			item = 0
			item += 1 until boolean_array[item] == true
			if dice[item] == 1
				score += 1000
			else
				score += dice[item] * 100
			end
		end

		# individual numbers
		dice.each_index do |i|
			unless boolean_array[i]
				if dice[i] == 1
					score += 100
				elsif dice[i] == 5
					score += 50
				end
			end
		end

		return score
	end

	def mark_repeated_numbers (array, number, times)
		boolean_array = Array.new(array.length, false)
		count = 0
		for i in (0...array.length)
			break if count == times
			if array[i] == number
				boolean_array[i] = true
				count += 1
			end
		end
		return [] if count < times
		return boolean_array
	end

end


# Main program
game = Greed.new(2)
game.play
