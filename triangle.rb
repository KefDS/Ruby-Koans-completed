# Triangle Project Code.

# Triangle analyzes the lengths of the sides of a triangle
# (represented by a, b and c) and returns the type of triangle.
#
# It returns:
#   :equilateral  if all sides are equal
#   :isosceles    if exactly 2 sides are equal
#   :scalene      if no sides are equal
#
# The tests for this method can be found in
#   about_triangle_project.rb
# and
#   about_triangle_project_2.rb
#
def triangle(a, b, c)
  # WRITE THIS CODE
  a, b, c = [a, b, c].sort
  raise TriangleError, "Un triángulo no puede tener lados negativos o lado 0" if a <= 0
  raise TriangleError, "Any two sides of a triangle should add up to more than the third side." unless a + b > c

  if a == b or b == c or a == c
    if a == c and b == c
      :equilateral
    else
      :isosceles
    end
  else
    :scalene
  end
end

# Error class used in part 2.  No need to change this code.
class TriangleError < StandardError
end
